<?php


/**
 * An object containing all the data and member functions required for a widget.
 */
class embed_widget {
  public $wid;
  public $uid;
  public $title;
  public $description;
  public $recommended;
  public $source_type = NULL;
  public $source = NULL;
  protected $settings = array();
  protected $cache_id;
  
  public $format = 'iframe';
  public $platform = 'html';
  
  public $params = array();
    
  /**
   * Constructor. Accepts either a numeric or encrypted widget ID.
   *
   * @param $wid
   *   Numeric or encrypted widget ID.
   * @param $iid
   *   Optional. Instance ID.
   */
  function __construct($wid = NULL) {
    if ($wid) {
      if (is_numeric($wid)) {
        if (!$this->object_load($wid)) {
          drupal_set_message(t('Widget could not be loaded.'), 'warning');
        }
      }
      else if (is_string($wid)) {
        $this->decode($wid);
      }
    }
    else {
      global $user;
      $this->uid = $user->uid;
      $this->settings = embed_widget::default_settings();
    }
  }
  
  /**
   * Sets $wid attribute.
   *
   * @param $wid
   *   Widget ID as an integer.
   */
  public function set_wid($wid) {
    if (is_numeric($wid) || $wid == 'preview') {
      $this->wid = $wid;
    }
    else {
     drupal_set_message(t('wid must be an integer.'), 'warning');
    }
  }
  
  /**
   * Sets $uid attribute.
   *
   * @param $uid
   *   User ID as an integer.
   */
  public function set_uid($uid) {
    if (is_numeric($uid)) {
      $this->uid = $uid;
    }
    else {
     drupal_set_message(t('uid must be an integer, %type given.', array('%type' => gettype($uid))), 'warning');
    }
  }
  
  /**
   * Sets $description attribute.
   *
   * @param $desc
   *   A description of the widget.
   */
  public function set_description($desc) {
    $this->description = $desc;
  }
   
  public function get_cache_id() {
    return $this->cache_id;
  }
  
  /**
   * Loads a widget object.
   *
   * @param $wid
   *   Numeric widget ID.
   * @return
   *   TRUE on success; FALSE on failure.
   */
  protected function object_load($wid) {
    if (!is_numeric($wid)) {
      return FALSE;
    }
    $embed_widget = embed_widget::db_load($wid);
    //$instance = embed_widget::load_instance($iid);

    if ($embed_widget) {
      foreach ($embed_widget as $attribute => $value) {
        $this->$attribute = $value;
      }
      //print "Loading!" . "<br>";
      //print Print_r($embed_widget, TRUE);
      return TRUE;
    }
    else {
      // TODO: Load widget default values.
      $this->settings = embed_widget::default_settings();
      return FALSE;
    }
  }
  
  static public function default_settings($settings = NULL, $refresh = FALSE) {
    if ($refresh) {
      variable_del('embed_widgets_widget_defaults');
    }
    else if ($settings && is_array($settings)) {
      return variable_set('embed_widgets_widget_defaults', $settings);
    }
    return variable_get('embed_widgets_widget_defaults', array('width' => 300, 'height' => 300));
  }

  /**
   * Loads widget information from the database.
   *
   * @param $wid
   *   Numeric widget ID to be loaded or widget object to be cached.
   * @param $refresh
   *   If TRUE, refreshes cache of loaded widgets.
   * @return
   *   A widget object from the database.
   */
  protected static function db_load($wid, $refresh = FALSE) {
    static $embed_widgets = array();
    if (!is_numeric($wid)) {
      if (is_object($wid) && isset($wid->wid)) {
        $embed_widgets[$wid->wid] = $wid;
        return $embed_widgets[$wid->wid];
      }
      else if ($wid === 'all') {
        $result = db_query("SELECT * FROM {embed_widgets}");
        while ($embed_widget = db_fetch_array($result)) {
          $embed_widget->source = unserialize($embed_widget->source);
          $embed_widget->settings = unserialize($embed_widget->settings);
          $embed_widgets[$wid] = $embed_widget;
        }
        return $embed_widgets;
      }
      return FALSE;
    }
    if ($refresh) {
      $embed_widgets = array();
    }
  
    if (!isset($embed_widgets[$wid])) {
      $result = db_query("SELECT * FROM {embed_widgets} WHERE wid = %d", $wid);
      if ($result) {
        $embed_widget = db_fetch_object($result);
      }
      if ($embed_widget) {
        $embed_widget->source = unserialize($embed_widget->source);
        $embed_widget->settings = unserialize($embed_widget->settings);
        $embed_widgets[$wid] = $embed_widget;
      }
      else {
        return FALSE;
      }
    }
  
    return $embed_widgets[$wid];
  }
  
  /**
   * Loads widget information from the cache.
   *
   * @param $cache_id
   *   A cache id.
   */
  protected function cache_load($cache_id) {
    $cache = cache_get('embed_widgets_' . $cache_id);
    //$embed_widget = isset($cache->data) ? $cache->data : NULL;
    if ($cache->data) {
      foreach ($cache->data as $attribute => $value) {
        $this->$attribute = $value;
      }
      return TRUE;
    }
    return FALSE;
  }
  
  /**
   * Saves widget to the database.
   * 
   * @return 
   *   Either SAVED_UPDATED or SAVED_NEW.
   */
  public function save() {
    if (isset($this->wid) && is_numeric($this->wid)) {
      db_query("UPDATE {embed_widgets} SET uid = '%d', status = '%d', title = '%s', description = '%s', source = '%s', settings = '%s' WHERE wid = %d", $this->uid, $this->recommended, $this->title, $this->description, serialize($this->source), serialize($this->settings), $this->wid);
      $return = SAVED_UPDATED;
      embed_widget::db_load($this);
    }
    else {
      db_query("INSERT INTO {embed_widgets} (uid, status, title, description, source, settings) VALUES ('%d', '%d', '%s', '%s', '%s', '%s')", $this->uid, $this->recommended, $this->title, $this->description, serialize($this->source), serialize($this->settings));
      $this->wid = db_last_insert_id("embed_widgets", "wid");
      $return = SAVED_NEW;
      embed_widget::db_load($this);
      
      $placement = new embed_widget_placement($this->wid);
      $placement->parent = 0;
      $placement->save();
      //TODO: Save default placement.
    }
    if ($this->source) {
      $this->source->save($this->wid, $return);
    }
    return $return;
  }
  
  /**
   * Deletes widget from the database.
   * 
   * @return 
   *   TRUE on success; FALSE on failure.
   */
  public function delete() {
    if ($this->object_load(($this->wid))) {
      db_query("DELETE FROM {embed_widgets} WHERE wid = %d", $this->wid);
      db_query("DELETE FROM {embed_widgets_placement} WHERE wid = %d", $this->wid);
      db_query("DELETE FROM {embed_widgets_source_map} WHERE wid = %d", $this->wid);
      return TRUE;
    }
    return FALSE;
  }
  
  static public function lookup($type, $id, $delta = NULL) {
    if ($type == 'node' && is_object($id)) {
      $id = $id->nid;
    }
    if ($delta == NULL) {
      $result = db_query("SELECT wid FROM {embed_widgets_source_map} WHERE type = '%s' AND foreign_id = '%s'", $type, $id);
    }
    else {
      $result = db_query("SELECT wid FROM {embed_widgets_source_map} WHERE type = '%s' AND foreign_id = '%s' AND delta = '%s'", $type, $id, $delta);
    }
    $wid = db_result($result);
    if ($wid) {
      return embed_widgets_load($wid);
    }
    return FALSE;
  }
   
  /**
   * Encrypts widget information to be placed securely in a URL.
   * 
   * @return
   *   The encoded widget as a string.
   */
  public function encode() {
    if (!isset($this->encoded)) {
      $wid_length = isset($this->wid) ? strlen(strval($this->wid)): 0;
      $iid_length = isset($this->iid) ? strlen(strval($this->iid)): 0;
      $this->encoded = $this->hash($this->wid, $this->iid);
      $this->encoded .= '-' . $wid_length . '-' . $iid_length;
      
      if (isset($this->cache_id)) {
        $this->encoded .= '-' . $this->cache_id;
      }
    }
    return $this->encoded;
  }
  
  /**
   * Decrypt an encrypted widget and loads the widget object.
   *
   * @param $encoded_widget
   *   Encrypted widget information as a string.
   * @return
   *   TRUE on success; FALSE on failure.
   */
  protected function decode($encoded_widget) {
    // Decode widget.
    list($hash, $wid_length, $iid_length, $cache_id) = split("-", $encoded_widget);
    
    if ($cache_id && $this->cache_load($cache_id)) {    
      return TRUE;
    }
    else if (!$cache_id) {
      $wid = $wid_length ? substr($hash, 7, $wid_length) : NULL;
      $iid = $iid_length ? substr($hash, 20, $iid_length) : NULL;
      if ($this->hash($wid, $iid) === $hash) {
        $this->object_load($wid, $iid);
        return TRUE;
      }
    }
    return FALSE;
  }
  
  /**
   * Helper function used for encrypting and decrypting widgets.
   * 
   * @param $wid
   *   Widget ID as an integer.
   * @param $iid
   *   Widget instance ID as an integer.
   * @return
   *   The hashed widget as a string.
   */
  protected function hash($wid=NULL, $iid = NULL) {
    static $hash = NULL;
    if ($hash == NULL) {
      global $cookie_domain, $base_path;
      $salt = substr($cookie_domain, 1);
      $wid_length = count($wid);
      $iid_length = count($iid);
  
      $hash = substr_replace(base64_encode(crypt($wid . $base_path . $iid, $salt) . $cookie_domain), $wid, 7, $wid_length);
      if ($iid_length) {
        $hash = substr_replace($hash, $iid, 20, $iid_length);
      }
      $hash = trim($hash, "=");
    }
    return $hash;
  }
  
  /**
   * Save a widget to the persistent cache.
   *
   * @param $form_build_id
   *   A Form API form build ID.
   */
  public function cache($cache_id) {
    $expire = 1800;
    $this->cache_id = $cache_id;
    cache_set('embed_widgets_' . $cache_id, $this, 'cache', time() + $expire);
  }
  
  /**
   * Set/create the widget source attribute.
   *
   * @param $type
   *   The type of widget source (e.g. "page", "block", "view", etc.).
   * @param $options
   *   An array of options passed to the source constructor.
   */
  public function set_source($type, $options = array()) {
    $this->source_type = $type;
    $type = $type . "_source";
    $this->source = new $type($options);
  }
  
  /**
   * Generate the content for this widget.
   *
   * @return
   *   The HTML content to be displayed in the widget.
   */
  public function build() {
    $content = array();
    $content['title'] = $this->title;
    $this->source->params = $this->params;
    
    if ($this->source->check_access()) {
      // User has permission to view content.
      $content['content'] = $this->source->get_content();
    }
    else {
      // Permission denied. Display login form.
      $logged_in = user_is_logged_in();
      $content['content'] = $logged_in ? 'You do not have permission to view this content.' : 'You must login to view this content.<br/>';
      if (!$logged_in) {
        $login_block = new block_source('user', 0);
        $login_block->set_ignore_visibility(TRUE);
        $content['content'] .= $login_block->get_content();
      }
    }
    return $content;
  }
    
  /**
   * Get the URL to a widget's embed file.
   *
   * @param $filename
   *   The filename to attach to the end of the URL.
   * @param $params
   *   Additional parameters to add to the URL.
   * @return
   *   The URL to a widget's embed file as a string.
   */
  public function get_embed_url($filename = "", $params = array()) {
    // embed-widgets/embed/$encoded_WID/$encoded_PID
    // WID = Widget ID, PID = Placement ID.  http://www.clearspring.com/docs/tech/core/wid-pid
    // With parameters: http://widgets.clearspring.com/o/4637cc0e7af63b5b/-/-/-/cFirstName/Angelina/cLastName/Jolie/widget.js
    // No PID = default configuration.
    $path = 'embed-widgets/embed/' . $this->encode() . '/';
    foreach ($params as $name => $value) {
      $path .= $name . '/' . $value . '/';
    }
    $path .= $filename;
    return url($path, array('absolute' => TRUE));
  }
  
  /**
   * Get the URL to a widget's content.
   *
   * @return
   *   The URL to a widget's content as a string.
   */
  public function get_content_url() {
      $path = 'embed-widgets/content/' . $this->encode() . '/';
//      foreach ($params as $name => $value) {
//        $path .= $name . '/' . $value . '/';
//      }
      return url($path, array('absolute' => TRUE));
  }
  
//  /**
//   * Get the embed code for this widget.
//   *
//   * @return
//   *   The HTML code required to embed this widget.
//   */
//  public function get_code() {
//    return call_user_func($this->platform . '::code', $this);
//  }
  
  /**
   * Set a persistent variable for this widget.
   *
   * @param $name
   *   The name of the variable being set.
   * @param $value
   *   The value of the variable being set.
   */
  public function set_variable($name, $value) {
      $this->settings[$name] = $value;
  }
  
  /**
   * Get a persistent variable for this widget.
   *
   * @param $name
   *   The name of the variable to return.
   * @return
   *   The value of the requested variable.
   */
  public function get_variable($name) {
      return $this->settings[$name];
  }
  
  /**
   * Get settings for this widget.
   *
   * @return
   *   The value of the requested variable.
   */
  public function get_settings() {
      return $this->settings;
  }
  
  /**
   * Set widget parameters.
   *
   * @param $value
   *   An array of parameters or the value of one parameter.
   * @param $params
   *   The name of the parameter given as $value.
   */
  public function set_param($value, $name = NULL) {
    if (is_array($value)) {
      $this->params = array_merge($this->params, $value);
    }
    else if (isset($name)) {
      $this->params[$name] = $value;
    }
    else {
      $this->params[] = $value;
    }
  }
  
  /**
   * Add widget parameter.
   *
   * @param $value
   *   An array of parameters or the value of one parameter.
   */
  public function add_param($value) {
    if (is_array($value)) {
      $this->params = $value; // TODO: Fix this.
    }
    else {
      $this->params[] = $value;
    }
  }

 static public function widget_attributes_form(embed_widget $widget = NULL) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => is_object($widget) ? $widget->title : NULL,
      '#size' => 30,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#description' => t('The widget title.'),
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => is_object($widget) ? $widget->description : NULL,
      '#size' => 50,
      '#maxlength' => 255,
      '#description' => t('A description of the widget, its content, and/or its purpose.'),
    );
    return $form;
  }
  
  static public function widget_settings_form($settings = NULL) {
    if (!$settings) {
      $settings = embed_widget::default_settings();
    }
    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $settings['width'],
      '#size' => 4,
      '#maxlength' => 4,
      '#field_suffix' => 'pixels',
      '#required' => TRUE,
      '#description' => t('The width of the widget in pixels.'),
    );
    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#default_value' => $settings['height'],
      '#size' => 4,
      '#maxlength' => 5,
      '#field_suffix' => 'pixels',
      '#required' => TRUE,
      '#description' => t('The height of the widget in pixels.'),
    );
    return $form;
  }
}

