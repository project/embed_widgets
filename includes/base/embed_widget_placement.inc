<?php

class embed_widget_placement extends embed_widget {
  public $iid;
  public $parent;
  public $template;
  public $format;
  public $platform;
  public $params = array();
  public $instance_settings;
  public $installs;
  public $views;
  
  /**
   * Constructor. Accepts either a numeric or encrypted widget ID.
   *
   * @param $wid
   *   Numeric or encrypted widget ID.
   * @param $iid
   *   Optional. Instance ID.
   */
  function __construct($wid = NULL, $iid = NULL) {
    if ($wid) {
      if (is_numeric($wid)) {
        if (!$this->object_load($wid, $iid)) {
          drupal_set_message(t('Widget placement could not be loaded: ') . $wid . ' ' . $iid, 'warning');
        }
      }
      else if (is_string($wid)) {
        $this->decode($wid);
      }
    }
    else {
      global $user;
      $this->uid = $user->uid;
    }
  }
  
  
  /**
   * Loads a widget placement object.
   *
   * @param $wid
   *   Numeric widget ID.
   * @param $iid
   *   Optional. Instance ID.
   * @return
   *   TRUE on success; FALSE on failure.
   */
  protected function object_load($wid, $iid = NULL) {
    
    if (!is_numeric($wid)) {
      return FALSE;
    }

    if (!parent::object_load($wid)) {
      return FALSE;
    }

    if ($iid) {
      $instance = $this->db_load($iid);
  
      if ($instance) {
        $this->iid = $instance['iid'];
        $this->parent = $instance['parent'];
        $this->template = $instance['template'];
        $this->format = $instance['format'];
        $this->platform = $instance['platform'];
        $this->instance_settings = array_merge($this->settings, $instance['settings']);
        //$this->installs = $instance['installs'];
        //$this->views = $instance['views'];
      }
      else {
        return FALSE;
      }
    }

    return TRUE;
  }
  
  /**
   * Loads widget instance information from the database.
   *
   * @param $iid
   *   Numeric instance ID to be loaded.
   * @param $refresh
   *   If TRUE, refreshes cache of loaded widgets.
   * @return
   *   An array of instance information from the database.
   */
  protected static function db_load($iid, $refresh = FALSE) {
    static $embed_widget_instances = array();
    if (!is_numeric($iid)) {
        return FALSE;
    }
    if ($refresh) {
      $embed_widget_instances = array();
    }
  
    if (!isset($embed_widget_instances[$iid])) {
      $result = db_query("SELECT * FROM {embed_widgets_placement} WHERE iid = %d", $iid);
      if ($result) {
        $instance = db_fetch_array($result);
      }
      if ($instance) {
        $instance['settings'] = unserialize($instance['settings']);
        $embed_widget_instances[$iid] = $instance;
      }
      else {
        return FALSE;
      }
    }
  
    return $embed_widget_instances[$iid];
  }
  
  
    /**
   * Saves widget instance to the database.
   * 
   * @return 
   *   Either SAVED_UPDATED or SAVED_NEW.
   */
  public function save($save_widget = FALSE) {
    if ($save_widget) {
      parent::save();
    }
    if (is_numeric($this->iid) && is_numeric($this->wid)) {
      db_query("UPDATE {embed_widgets_placement} SET wid = '%d', parent = '%d', format = '%s', platform = '%s', template = '%s', settings = '%s' WHERE iid = %d", $this->wid, $this->parent, $this->format, $this->platform, $this->template, serialize($this->settings), $this->iid);
      $return = SAVED_UPDATED;
    }
    else if (is_numeric($this->wid) && is_string($this->format) && is_string($this->platform)) {
      db_query("INSERT INTO {embed_widgets_placement} (wid, parent, format, platform, template, settings) VALUES ('%d', '%d', '%s', '%s', '%s', '%s')", $this->wid, $this->parent, $this->format, $this->platform, $this->template, serialize($this->settings));
      $this->iid = db_last_insert_id("embed_widgets_placement", "iid");
      $return = SAVED_NEW;
    }
    return $return;
  }
  
  /**
   * Deletes widget instance from the database.
   * 
   * @return 
   *   TRUE on success; FALSE on failure.
   */
  public function delete() {
    if ($this->object_load($this->wid, $this->iid)) {
      db_query("DELETE FROM {embed_widgets_placement} WHERE iid = %d", $this->iid);
      return TRUE;
    }
    return FALSE;
  }
    
  static public function load_root($wid) {
    if (is_numeric($wid)) {
      $result = db_query("SELECT iid FROM {embed_widgets_placement} WHERE wid = '%d' AND parent = '%d'", $wid, 0);
    }
    $iid = db_result($result);
    if ($iid) {
      return embed_widgets_load($wid, $iid);
    }
    return FALSE;
  }
  
  public function create_child() {
    if (!is_numeric($this->iid)) {
      return FALSE;
    }
    $child = new embed_widget_placement($this->wid, $this->iid);
    unset($child->iid);
    $child->parent = $this->iid;
    $child->save();
    return $child;
  }
  
  /**
   * Get the embed code for this widget.
   *
   * @return
   *   The HTML code required to embed this widget.
   */
  public function get_code() {
    return call_user_func($this->platform . '::code', $this);
  }
  
  /**
   * Add CSS specific to widget's format.
   */
  public function add_css() {
    $info = call_user_func($this->format . '::info');
    foreach ($info['css'] as $css_file) {
      drupal_add_css($info['path'] . '/' . $css_file);
    }
  }
  
}