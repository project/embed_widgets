<?php

/**
 * An object containing all the data and member functions to provide view content to a widget.
 */
class view_source extends widget_source {
  
  static public function info() {
    if (module_exists('views')) {
      return array(
        'title' => t('View'),
        'description' => t('Allows Views to be used in embed widgets.'),
      );
    }
  }
  
  public function set_view($view) {
    if (is_string($view)) {
      $this->settings['view'] = $view;
    }
    else {
      drupal_set_message(t('Widget view source: View name must be a string.'), 'warning');
    }
  }
  
  public function set_display($display) {
    if (is_string($display)) {
      $this->settings['display'] = $display;
    }
    else {
      drupal_set_message(t('Widget view source: Display name must be a string.'), 'warning');
    }
  }
  
  public function set_arguments($args) {
    if (is_array($args)) {
      $this->params = $args;
    }
    else {
      drupal_set_message(t('View arguments must be an array.'), 'warning');
    }
  }
  
  protected function build() {
    if (module_exists('views')) {
      $view = views_get_view($this->settings['view']);
      if ($view) {
        $args = is_array($this->params) ? $this->params : array();
        return $view->preview($this->settings['display'], $args);
      }
      return t('The view could not be loaded.');
    }
    drupal_set_message(t('Views module must be enabled to display views in widgets.'), 'warning');
    return t('Error loading content.');;
  }
  
  public function check_access() {
    $view = views_get_view($this->settings['view']);
    return $view->access($this->settings['display']);
  }
  
}

