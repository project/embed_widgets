<?php

/**
 * An object containing all the data and member functions to provide node content to a widget.
 */
class node_source extends widget_source {
  
  protected $node;
   
  static public function info() {
    return array(
      'title' => t('Node'),
      'description' => t('Allows any node to be embedded as a widget.'),
    );
  }
  
  public function set_nid($nid) {
    if (is_numeric($nid)) {
      $this->settings['nid'] = $nid;
    }
    else if (is_string($nid)) {
      $path = drupal_get_normal_path($nid);
      list($node, $nid) = split("/", $path);
      if ($node == 'node' && is_numeric($nid)) {
        $this->settings['nid'] = $nid;
      }
    }
    else {
      drupal_set_message(t('Widget node source: Node ID or path alias must be given.'), 'warning');
    }
  }
   
 /**
   * Saves widget source information to the database.
   * 
   * @return 
   *   Either SAVED_UPDATED or SAVED_NEW.
   */
  public function save($wid, $query_type) {
    $type = 'node';
    if (is_numeric($this->settings['nid']) && is_numeric($wid)) {
      if ($query_type == SAVED_NEW) {
        db_query("INSERT INTO {embed_widgets_source_map} (type, foreign_id, wid) VALUES ('%s', '%s', '%d')", $type, $this->settings['nid'], $wid);
      }
      else if ($query_type == SAVED_UPDATED) {
        db_query("UPDATE {embed_widgets_source_map} SET type = '%s', foreign_id = '%s', delta = NULL, wid = '%d' WHERE wid = %d", 'node', $this->settings['nid'], $wid);
      }
      return $query_type;
    }
    return FALSE;
  }
  
  public function build() {
    if ($this->load_node()) {
      return node_view($this->node, FALSE, TRUE);
    }
    return FALSE;
  }
  
  protected function load_node() {
    if (!isset($this->settings['nid'])) {
      return FALSE;
    }
    if  (!isset($this->node)) {
      $this->node = node_load($this->settings['nid']);
    }
    return TRUE;
  }
  
  public function check_access() {
    if ($this->load_node()) {
      return node_access('view', $this->node);
    }
    return FALSE;
  }
  
  static public function admin_settings_form() {
    $form = array();
    $form['node'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node Widgets'),
      '#collapsible' => TRUE,
    );
    $content_types = array();
    foreach (node_get_types() as $key => $type) {
      $content_types[$key] = $type->name;
    }
    $form['node']['embed_widgets_content_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content Types'),
      '#description' => t('Check the box next to each type of content that you would like to be able to use as a widget.'),
      '#options' => $content_types,
      '#default_value' => variable_get('embed_widgets_content_types', array()),
    );
    return $form;
  }
}