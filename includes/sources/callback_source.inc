<?php

/**
 * An object containing all the data and member functions to allow a user function to provide widget content.
 */
class callback_source extends widget_source {
    
  static public function info() {
    return array(
      'title' => t('Callback'),
      'description' => t('Allows modules to define content to be embedded in widgets.'),
    );
  }
  
  public function set_callback($callback) {
    if (is_string($callback)) {
      $this->settings['callback'] = $callback;
    }
    else {
      drupal_set_message(t('%callback is not a valid widget source callback function.', array('%callback' => $callback)), 'warning');
    }
  }
  
  public function set_access_callback($callback) {
    if (is_string($callback)) {
      $this->settings['access_callback'] = $callback;
    }
    else {
      drupal_set_message(t('%callback is not a function. Must be a string.', array('%callback' => $callback)), 'warning');
    }
  }
  
  public function set_path($path) {
    if (is_string($path)) {
      $this->settings['path'] = $path;
    }
    else {
      drupal_set_message(t('%path is not a valid path. Must be a string.', array('%path' => $path)), 'warning');
    }
  }
  
  public function set_file($file) {
    if (is_string($file)) {
      $this->settings['file'] = $file;
    }
    else {
      drupal_set_message(t('%file is not a valid file name. Must be a string.', array('%file' => $file)), 'warning');
    }
  }
  
  public function set_arguments($args) {
    if (is_array($args)) {
      $this->params = $args;
    }
    else {
      drupal_set_message(t('Callback arguments must be an array.'), 'warning');
    }
  }
  
  public function set_callback_include($path, $file) {
    if (is_string($path) && is_string($file)) {
      $this->settings['file'] = $file;
      $this->settings['path'] = $path;
    }
    else {
      drupal_set_message(t('Widget callback source: Path and file must be strings.'), 'warning');
    }
  }
  
  protected function build() {
    if (isset($this->settings['file']) && isset($this->settings['path'])) {
      require_once './' . $this->settings['path'] . "/$this->settings['file']";
    }
    if (isset($this->settings['callback']) && function_exists($this->settings['callback'])) {
      if (!isset($this->params) || !is_array($this->params)) {
        $this->params = array();
      }
      return call_user_func_array($this->settings['callback'], $this->settings['args']);
    }
    drupal_set_message(t('%callback is not a valid widget source callback function. Function does not exist.', array('%callback' => $callback)), 'warning');
    return NULL;
  }
  
  public function check_access() {
    if (isset($this->settings['access_callback'])) {
      return call_user_func($this->settings['access_callback']);
    }
    return TRUE;
  }
}