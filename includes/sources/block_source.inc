<?php

require_once drupal_get_path('module', 'embed_widgets') ."/embed_widgets.module";
spl_autoload_register('embed_widgets_autoload');

/**
 * An object containing all the data and member functions to provide block content to a widget.
 */
class block_source extends widget_source {
  
  static public function info() {
    return array(
      'title' => t('Block'),
      'description' => t('Allows any block to be embedded as a widget.'),
    );
  }
  
  public function set_block($module, $delta) {
    if (is_string($module) && (is_string($delta) || is_numeric($delta))) {
      $this->settings['module'] = $module;
      $this->settings['delta'] = $delta;
    }
    else {
      drupal_set_message(t('Invalid module or delta given for widget block source.'), 'warning');
    }
  }
  
  public function set_ignore_visibility($ignore) {
    if (is_bool($ignore)) {
      $this->settings['ignore'] = $ignore;
    }
  }
  
  protected function build() {
    global $theme_key;
    
    if (isset($this->settings['module']) && isset($this->settings['delta'])) {
      $module = $this->settings['module'];
      $delta = $this->settings['delta'];
      
      if ($this->settings['ignore']) {
        $block = module_invoke($module, 'block', 'view', $delta);
        return $block['content'];
      }
  
      if (!isset($theme_key)) {
        init_theme();
      }
      $region = db_result(db_query("SELECT region FROM {blocks} WHERE module = '%s' AND delta = '%s' AND theme = '%s'", $module, $delta, $theme_key));
      $blocks = block_list($region);
  
      if (isset($blocks[$module .'_'. $delta])) {
        $block = $blocks[$module .'_'. $delta];
        return $block->content;
      }
    }
    
    return t('Invalid block.');
  }
  
 /**
   * Saves widget source information to the database.
   * 
   * @return 
   *   Either SAVED_UPDATED, SAVED_NEW, or FALSE.
   */
  public function save($wid, $query_type) {
    $type = 'block';
    if (isset($this->settings['module']) && isset($this->settings['delta']) && is_numeric($wid)) {
      if ($query_type == SAVED_NEW) {
        db_query("INSERT INTO {embed_widgets_source_map} (type, foreign_id, delta, wid) VALUES ('%s', '%s', '%s', '%d')", $type, $this->settings['module'], $this->settings['delta'], $wid);
      }
      else if ($query_type == SAVED_UPDATED) {
        db_query("UPDATE {embed_widgets_source_map} SET type = '%s', foreign_id = '%s', delta = '%s', wid = '%d' WHERE wid = %d", 'node', $this->settings['nid'], $this->settings['delta'], $wid);
      }
      return $query_type;
    }
    return FALSE;
  }
  
  public function check_access() {
    global $theme_key;
    $module = $source['settings']['module'];
    $delta = $source['settings']['delta'];
    if (!isset($theme_key)) {
      init_theme();
    }
    $region = db_result(db_query("SELECT region FROM {blocks} WHERE module = '%s' AND delta = '%s' AND theme = '%s'", $module, $delta, $theme_key));
    $blocks = block_list($region);
    if (isset($blocks[$module .'_'. $delta])) {
      return TRUE;
    }
    return FALSE;
  }
}