<?php

/**
 * An object containing all the data and member functions to provide page content to a widget.
 */
class page_source extends widget_source {
   
  static public function info() {
    return array(
      'title' => t('Page'),
      'description' => t('Allows any page to be embedded as a widget.'),
    );
  }
  
  public function set_path($path) {
    if (is_string($path)) {
      $this->settings['path'] = $path;
    }
    else {
      drupal_set_message(t('Widget page source must be a string.'), 'warning');
    }
  }
   
  protected function build() {
    // Display a page
    // Adapted from print.module.
    $path = $this->settings['path'];
    $last = strlen($path);
    if ($path[$last] != '/') {
      $path .= '/';
    }
    foreach ($this->params as $value) {
      $path .= $value . '/';
    }
    $path = drupal_get_normal_path($this->settings['path']);
    
    
  
    menu_set_active_item($path);
    $node = new stdClass();
    $node->body = menu_execute_active_handler($path);
    $node->title = $this->title;
    $node->path = $path;
        
    // It may happen that a drupal_not_found is called in the above call
    if (preg_match('/404 Not Found/', drupal_get_headers()) == 1) {
      return;
    }
    
    // Delete any links area
    $node->body = preg_replace("/\s*<div class=\"links\">.*?<\/div>/sim", "", $node->body);

    $path = empty($node->nid) ? $node->path : "node/$node->nid";
  
    if (isset($node->type)) {
      $node_type = $node->type;
  
      $by_author = ($node->name ? $node->name : variable_get('anonymous', t('Anonymous')));
      $by = t('By %author', array('%author' => $by_author));
      $page["submitted"] = theme_get_setting("toggle_node_info_$node_type") ? $by : "";
  
      $created_datetime = format_date($node->created, 'small');
      $created = t('Created %date', array('%date' => $created_datetime));
      $page["created"] = theme_get_setting("toggle_node_info_$node_type") ? $created : "";
  
      $page["type"] = $node->type;
    }
    else {
      $page["submitted"] = "";
      $page["created"] = "";
      $page["type"] = '';
    }
  
    menu_set_active_item($path);
    $breadcrumb = drupal_get_breadcrumb();
    if (!empty($breadcrumb)) {
      $breadcrumb[] = menu_get_active_title();
      $page["breadcrumb"] = implode(" > ", $breadcrumb);
    }
    else {
      $page["breadcrumb"] = "";
    }
  
    if (module_exists('taxonomy')) {
      $terms = taxonomy_link('taxonomy terms', $node);
      $page["taxonomy"] = theme('links', $terms);
    }
  
    $page["content"] = $node->body;
    
    switch ($node->body) {
      case MENU_NOT_FOUND:
        $page['title'] = t('Error 404');
        $page['content'] = t('The requested content could not be found.');
        break;
      case MENU_ACCESS_DENIED:
        $page['title'] = t('Access Denied');
        $page['content'] = t('You must !link to view this content.', array('!link' => l('login', 'user')));
        break;
    }
    
    return $page['content'];
  }
  
  public function check_access() {
    $path = drupal_get_normal_path($this->settings['path']);
    $router_item = menu_get_item($path);
    if ($router_item) {
      return $router_item['access'];
    }
    return FALSE;
  }
}