<?php

class html extends widget_platform {
    
  static public function info() {
    return array(
      'title' => t('HTML'),
      'description' => t('Embed on your website or blog.'),
      'formats' => array('iframe'),
    );
  }
  
  static public function theme() {
    return array();
  }
  
  static public function code($widget) {
    //$format_info = call_user_func($this->format . '::info');
    switch($widget->format) {
      case 'iframe':
        $code = call_user_func($widget->format . '::embed_code', $widget);
        break;
      case 'flash':
        $code = '';
        break;
    }
    
    return $code;
  }
  
  static public function live_preview($widget) {
    switch($widget->format) {
      case 'iframe':
        if (function_exists($widget->format . '::embed_code_noscript')) {
          $code = call_user_func($widget->format . '::embed_code_noscript', $widget);
        }
        break;
      case 'flash':
        $code = '';
        break;
    }
    
    return $code;
  }
    
//  public function settings_form() {  }
//  public function admin_settings() {  }
  
}

