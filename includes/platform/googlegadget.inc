<?php

//function embed_widgets_googlegadget_info() {
//  return "I am Google Gadget!";
//}


class googlegadget extends widget_platform {
  
  public $name;
  public $id;
  public $description;
  public $format;
  public $filename;
   
  static public function info() {
    return array(
      'title' => t('Google Gadget'),
      'description' => t('Embed in iGoogle, Open Social containers, or any other Google Gadget container.'),
      'formats' => array('iframe'),
    );
  }
  
  static public function theme() {
    return array(
      'googlegadget' => array(
        'template' => 'googlegadget',
        'arguments' => array('widget' => NULL),
        'path' => drupal_get_path('module', 'embed_widgets') . "/includes/platform"
      ),
    );
  }
  
  static public function code($widget) {
    //$format_info = call_user_func($this->format . '::info');
    switch($widget->format) {
      case 'iframe':
        $code = '<script src="http://www.gmodules.com/ig/ifr?url=' . $widget->get_content_url() . '&amp;synd=open&amp;w=' . $widget->get_variable('width') . '&amp;h=' . $widget->get_variable('height') . '&amp;title=' . $widget->get_variable('title') . '&amp;border=%23ffffff%7C3px%2C1px+solid+%23999999&amp;output=js"></script>';
        break;
      case 'flash':
        $code = '';
        break;
    }
    
    return $code;
  }
  
  static public function live_preview($widget) {
    switch($widget->format) {
      case 'iframe':
        if (function_exists($widget->format . '::embed_code_noscript')) {
          $code = call_user_func($widget->format . '::embed_code_noscript', $widget);
        }
        break;
      case 'flash':
        $code = '';
        break;
    }
    
    return $code;
  }
  
  static public function platform_file($widget) {
    drupal_set_header('Content-type: text/xml; charset=utf-8');
    return theme('googlegadget', $widget);
  }

}



/**
 * Process variables for embed_widgets_google_gadgets_page.tpl.php
 */
function template_preprocess_googlegadget(&$variables) {
  $widget = $variables['widget'];
  $variables['title'] = check_plain($widget->title);
  $variables['description'] = check_plain($widget->description);
  //$variables['title_url'] = check_plain($variables['content']['settings']['google-gadget']['title_url']);
  $variables['height'] = check_plain($widget->get_variable('height'));
  $variables['width'] = check_plain($widget->get_variable('width'));
//  $variables['screenshot'] = check_plain($variables['content']['settings']['google-gadget']['screenshot']);
//  $variables['thumbnail'] = check_plain($variables['content']['settings']['google-gadget']['thumbnail']);
//  $variables['author'] = check_plain($variables['content']['settings']['google-gadget']['author']);
//  $variables['author_email'] = check_plain($variables['content']['settings']['google-gadget']['author_email']);
//  $variables['author_location'] = check_plain($variables['content']['settings']['google-gadget']['author_location']);
  $variables['url'] = check_plain($widget->get_content_url());
}