
Drupal.embedWidgets = {};

Drupal.embedWidgets.Attach = function (context) {

  // $("#myid", top.document); // Access elements outside of iframe.

  // Attach only if we are in a frame.
  if (top != self) {
    $('a').each(function() {
      var href = $(this).attr('href');
      var target = $(this).attr('target');

      if (href && target != '_blank') {
        // Open node and external links in a new window.
        //if ((href.indexOf('node/') != -1) || (href.indexOf('://') != -1)) {
        //  // $(this).attr('target', '_parent');
        //  $(this).attr('target', '_blank');
        //}
        //else {
          // Reconstruct the href with a embed_widgets_mode property in the querystring.
          var separator = href.indexOf('?') == -1 ? '?' : '&';
          // If there is a fragment, we need to insert the querystring part
          // before it.
          if (href.indexOf('#') != -1) {
            href = href.substring(0, href.indexOf('#')) + separator + 'widgets_mode=' + Drupal.settings.widgetID + href.substring(href.indexOf('#'));
          }
          else {
            href += separator + 'widgets_mode=' + Drupal.settings.widgetID;
          }
          $(this).attr('href', href);
        //}
      }
    });
    // Add an embed_widgets_mode element to all forms so that submission
    // will trigger embed widget content.
    $('form').append('<input type="hidden" name="widgets_mode" value="' + Drupal.settings.widgetID + '">');
  }
};

if (Drupal.jsEnabled) {
  $(document).ready(Drupal.embedWidgets.Attach);
}